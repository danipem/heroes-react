import React, {useContext} from 'react'
import { AuthContext } from '../../auth/AuthContext'
import { types } from '../../types/types';



export const LoginScreen = ( {history} ) => { // history se coge de las props (components en consola)

    const {dispatch} = useContext(AuthContext);
    
    const handleClick = () => {
        // history.push('/');
        // history.replace('/');

        const lastPath = localStorage.getItem('lastPath') || '/'; //para recordar la última página antes de logout

        dispatch({
            type: types.login,
            payload: {
                name: 'Daniel'
            }
        })
        history.replace( lastPath );
    }
    return (
        <div className="container mt-5">
            <h1>Login Screen</h1>


            <button
                className="btn btn-primary"
                onClick={handleClick}
            >
                Login
            </button>
        </div>
    )
}
